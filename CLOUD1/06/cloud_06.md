# 容器技术

[toc]

## 镜像编排

### Dockerfile语法

| 语法指令 | 语法说明                              |
| -------- | ------------------------------------- |
| FROM     | 基础镜像                              |
| RUN      | 制作镜像时执行的命令，可以有多个      |
| ADD      | 复制文件到镜像，自动解压              |
| COPY     | 复制文件到镜像，不解压                |
| EXPOSE   | 声明开放的端口                        |
| ENV      | 设置容器启动后的环境变量              |
| WORKDIR  | 定义容器默认工作目录（等于cd）        |
| CMD      | 容器启动时执行的命令，仅可以有一条CMD |


### 制作apache镜像

httpd.service 文件路径：/lib/systemd/system/httpd.service   
拷贝测试网页文件 public/info.php 到 /root/ 目录下

```dockerfile
[root@docker-0002 ~]# mkdir apache
[root@docker-0002 ~]# cp info.php apache/
[root@docker-0002 ~]# echo 'Welcome to The Apache.' >apache/index.html
[root@docker-0002 ~]# cd apache
[root@docker-0002 apache]# tar czf myweb.tar.gz index.html info.php
[root@docker-0002 apache]# vim Dockerfile
FROM mycentos:latest
RUN  yum install -y httpd php && yum clean all
ENV  LANG=C
ADD  myweb.tar.gz /var/www/html/
WORKDIR /var/www/html/
EXPOSE 80
CMD  ["/usr/sbin/httpd", "-DFOREGROUND"]
[root@docker-0002 apache]# docker build -t myapache:latest .
Successfully tagged myapache:latest
```

#### 验证镜像

```shell
[root@docker-0002 apache]# docker images
REPOSITORY   TAG       IMAGE ID       CREATED              SIZE
myapache     latest    da2fdea05a45   6 seconds ago        363MB
[root@docker-0002 apache]# docker rm -f $(docker ps -aq)
[root@docker-0002 apache]# docker run -itd --name myhttpd myapache:latest
[root@docker-0002 apache]# curl http://172.17.0.2/
Welcome to The Apache.
[root@docker-0002 apache]# docker rm -f myhttpd
myhttpd
```


### 制作phpfpm镜像

```dockerfile
[root@docker-0002 ~]# mkdir phpfpm
[root@docker-0002 ~]# cd phpfpm
[root@docker-0002 phpfpm]# vim Dockerfile
FROM mycentos:latest
RUN  yum install -y php-fpm && yum clean all
EXPOSE 9000
CMD ["/usr/sbin/php-fpm", "--nodaemonize"]
[root@docker-0002 phpfpm]# docker build -t phpfpm:latest .
Successfully tagged phpfpm:latest
```

#### 验证镜像

```shell
[root@docker-0002 phpfpm]# docker images
REPOSITORY   TAG       IMAGE ID       CREATED              SIZE
phpfpm       latest    133187726285   30 minutes ago       352MB
[root@docker-0002 phpfpm]# docker run -itd --name myphp phpfpm:latest
6eeff6af4a6469c298944b2bdd2ba69f32ebcbc6cb683a0a05af4eefbf90e8c1
[root@docker-0002 phpfpm]# docker exec -it myphp ss -ltun
Netid State      Recv-Q Send-Q       Local Address:Port      Peer Address:Port
tcp   LISTEN     0      128              127.0.0.1:9000                 *:*
[root@docker-0002 phpfpm]# docker rm -f myphp
myphp
```

### 制作nginx镜像

#### 编译软件包

```shell
[root@docker-0002 ~]# useradd nginx
[root@docker-0002 ~]# yum install -y gcc make pcre-devel openssl-devel
[root@docker-0002 ~]# tar zxf nginx-1.17.6.tar.gz
[root@docker-0002 ~]# cd nginx-1.17.6/
[root@docker-0002 nginx-1.17.6]# ./configure --prefix=/usr/local/nginx --user=nginx --group=nginx --with-http_ssl_module
[root@docker-0002 nginx-1.17.6]# make && make install
[root@docker-0002 nginx-1.17.6]# echo 'Nginx is running !' >/usr/local/nginx/html/index.html
```

#### 制作镜像


```dockerfile
[root@docker-0002 ~]# mkdir nginx
[root@docker-0002 ~]# cd nginx
[root@docker-0002 nginx]# tar czf nginx.tar.gz -C /usr/local nginx
[root@docker-0002 nginx]# vim Dockerfile 
FROM mycentos:latest
RUN  yum install -y pcre openssl && useradd nginx && yum clean all
ADD  nginx.tar.gz /usr/local/
WORKDIR /usr/local/nginx/html
EXPOSE 80
CMD  ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
[root@docker-0002 nginx]# docker build -t mynginx:latest .
Successfully tagged mynginx:latest
```

#### 验证镜像

```shell
[root@docker-0002 nginx]# docker images
REPOSITORY   TAG       IMAGE ID       CREATED          SIZE
mynginx      latest    f5cadb8aac31   10 minutes ago   323MB
[root@docker-0002 nginx]# docker run -itd --name mynginx mynginx:latest
e440b53a860a93cc2b82ad0367172c344c7207def94c4c438027c60859e94883
[root@docker-0002 nginx]# curl http://172.17.0.2/
Nginx is running !
[root@docker-0002 nginx]# docker rm -f mynginx
mynginx
```

## 微服务

### 对外发布服务

docker  run  -itd  -p 宿主机端口:容器端口  镜像名称:标签

```shell
# 把 docker-0002 绑定 apache 服务
[root@docker-0002 ~]# docker run -itd --rm -p 80:80 myapache:latest
# 在 docker-0002 上访问验证
[root@docker-0002 ~]# curl http://192.168.1.32/
Welcome to The Apache.

# 把 docker-0002 绑定 nginx 服务，删除 apache 的容器
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker run -itd --rm -p 80:80 mynginx:latest
# 在 docker-0002 上访问验证
[root@docker-0002 ~]# curl http://192.168.1.32/
Nginx is running !
```

### 容器共享卷

`docker run -itd -v 宿主机对象:容器内对象 镜像名称:标签  `

#### 共享网页目录

```shell
[root@docker-0002 ~]# mkdir /var/webroot
[root@docker-0002 ~]# echo "hello world" >/var/webroot/index.html
[root@docker-0002 ~]# cp info.php /var/webroot/
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker run -itd --rm --name mynginx \
                      -v /var/webroot:/usr/local/nginx/html mynginx:latest 
[root@docker-0002 ~]# curl http://172.17.0.2
hello world
[root@docker-0002 ~]# docker run -itd --rm --name myhttpd \
                      -v /var/webroot:/var/www/html myapache:latest
[root@docker-0002 ~]# curl http://172.17.0.3
hello world
```

#### 修改配置文件

```shell
[root@docker-0002 ~]# mkdir /var/webconf
[root@docker-0002 ~]# docker cp mynginx:/usr/local/nginx/conf/nginx.conf /var/webconf/
[root@docker-0002 ~]# vim /var/webconf/nginx.conf
        location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            include        fastcgi.conf;
        }
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker run -itd --name mynginx -p 80:80 \
                      -v /var/webconf/nginx.conf:/usr/local/nginx/conf/nginx.conf \
                      -v /var/webroot:/usr/local/nginx/html mynginx:latest
```

验证方式： 使用 exec 进入容器查看

```shell
[root@docker-0002 ~]# docker exec -it mynginx /bin/bash
[root@e440b53a860a html]# cat /usr/local/nginx/conf/nginx.conf
[root@e440b53a860a html]# # 查看 php 相关配置是否被映射到容器内
```

### 容器间网络通信

实验架构图例

```mermaid
flowchart LR
  subgraph docker-0002
      subgraph 容器1
        APP1[(Nginx)] & NET((共享网卡)) 
      end
      subgraph 容器2
        APP2[(PHP)]
      end
    APP1 & APP2 o---o NET((共享网卡)) & L((共享存储卷))
  end
U((用户)) --> APP1
classDef Docker fill:#ffffc0,color:#ff00ff
class docker-0002 Docker
classDef Container fill:#88aaff,color:#00ff00
class 容器1,容器2 Container
classDef DEV fill:#0f0fc0,color:#f0f000
class L DEV
```

实验步骤

```shell
[root@docker-0002 ~]# docker run -itd --rm --name myphp --network=container:mynginx \
                      -v /var/webroot:/usr/local/nginx/html phpfpm:latest
[root@docker-0002 ~]# docker exec -it mynginx ss -ltun
Netid  State      Recv-Q    Send-Q     Local Address:Port
tcp    LISTEN     0         128            127.0.0.1:9000
tcp    LISTEN     0         128                    *:80
[root@docker-0002 ~]# curl http://172.17.0.2/info.php
<pre>
Array
(
    [REMOTE_ADDR] => 172.17.0.1
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /info.php
)
php_host:       f705f89b45f9
1229
```

## docker私有仓库

### 私有仓库图例

```mermaid
flowchart LR
H1 o-..-o R([互联网仓库]):::Deploy
subgraph C[本地集群]
  I((本地仓库)):::Deploy
  H1[(容器服务<br>docker-0001)] & H2[(容器服务<br>docker-0002)] --> I
end
classDef Cluster fill:#ffffc0,color:#ff00ff,stroke-width:4px
class C Cluster
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
class H1,H2 Pod
classDef Deploy fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px  
```

### 私有仓库配置

| 主机名   | ip地址       | 最低配置    |
| -------- | ------------ | ----------- |
| registry | 192.168.1.30 | 2CPU,4G内存 |

```shell
[root@registry ~]# yum install -y docker-distribution
[root@registry ~]# systemctl enable --now docker-distribution
[root@registry ~]# curl http://127.0.0.1:5000/v2/_catalog
{"repositories":[]}
```

### 客户端配置

所有node节点都需要配置，这里 docker-0001，docker-0002都要配置

```shell
[root@docker-0001 ~]# vim /etc/hosts
192.168.1.30	registry
[root@docker-0001 ~]# vim /etc/docker/daemon.json
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "registry-mirrors": ["http://registry:5000","https://hub-mirror.c.163.com"],
    "insecure-registries":["192.168.1.30:5000","registry:5000"]
}
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)
[root@docker-0001 ~]# systemctl restart docker
```

#### 上传镜像

```shell
[root@docker-0001 ~]# docker tag myos:latest registry:5000/library/myos:latest
[root@docker-0001 ~]# docker push registry:5000/library/myos:latest
The push refers to repository [registry:5000/library/myos]
af5f00549b79: Pushed 
latest: digest: sha256:c130aafc1c2104f896ae03393740192d47 size: 527
[root@docker-0001 ~]# # 上传所有 myos 镜像
[root@docker-0001 ~]# docker tag centos:7 registry:5000/myimg/centos:7
[root@docker-0001 ~]# docker push registry:5000/myimg/centos:7
The push refers to repository [registry:5000/myimg/centos]
174f56854903: Pushed 
7: digest: sha256:dead07b4d8ed59d839686a31da35a3b532f size: 529
```

#### 验证测试

查看镜像名称： curl http://仓库IP:5000/v2/_catalog  
查看镜像标签： curl http://仓库IP:5000/v2/镜像名称/tags/list

```shell
[root@docker-0002 ~]# vim /etc/hosts
192.168.1.30	registry
[root@docker-0002 ~]# vim /etc/docker/daemon.json
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "registry-mirrors": ["http://registry:5000","https://hub-mirror.c.163.com"],
    "insecure-registries":["192.168.1.30:5000","registry:5000"]
}
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker rmi -f $(docker images -aq)
[root@docker-0002 ~]# systemctl restart docker
# 查看镜像名称
[root@docker-0002 ~]# curl http://registry:5000/v2/_catalog
{"repositories":["library/myos","myimg/centos"]}
# 查看镜像标签
[root@docker-0002 ~]# curl http://registry:5000/v2/library/myos/tags/list
{"name":"library/myos","tags":["latest","httpd","phpfpm","nginx","v2009"]}
# 仓库中的 library 是默认路径，允许容器不需要指明地址
[root@docker-0002 ~]# docker run -it myos:v2009
[root@634766f788d6 /]# 
# 如果不是默认路径，运行容器需要指明镜像地址
[root@docker-0002 ~]# docker run -it registry:5000/myimg/centos:7
[root@2b7cd6d88a76 /]# 
```

