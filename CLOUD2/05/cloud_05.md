# kubernetes

[toc]

## 卷管理

### 临时卷

Pod资源文件

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: web1
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: nginx
    image: myos:nginx
    ports:
    - protocol: TCP
      containerPort: 80
```

#### emptyDir卷

##### 临时空间

```shell
[root@master ~]# vim myv1.yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: web1
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  volumes:               # 卷配置
  - name: cache-volume   # 卷名称
    emptyDir: {}         # emptyDir 资源类型
  containers:
  - name: nginx
    image: myos:nginx
    volumeMounts:            # mount 卷
    - name: cache-volume     # 卷名称
      mountPath: /var/cache  # 路径
    ports:
    - protocol: TCP
      containerPort: 80

[root@master ~]# kubectl apply -f myv1.yaml
pod/web1 created
[root@master ~]# kubectl exec -it web1 -- /bin/bash
[root@web html]# df -h /var/cache 
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        40G  3.2G   35G   9% /var/cache

[root@master ~]# kubectl delete -f myv1.yaml
pod "web1" deleted
```

##### 共享数据

统计访问量（案例1）

```shell
[root@master ~]# vim myv1.yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: web1
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  volumes:
  - name: cache-volume
    emptyDir: {}
  - name: logs      # 卷名称
    emptyDir: {}    # emptyDir 资源类型
  containers:
  - name: nginx
    image: myos:nginx
    volumeMounts:
    - name: cache-volume
      mountPath: /var/cache
    - name: logs                        # 卷名称
      mountPath: /usr/local/nginx/logs  # 日志存储路径
    ports:
    - protocol: TCP
      containerPort: 80
  - name: log
    image: myos:v2009
    volumeMounts:           # 同一个 Pod 中的 Container
    - name: logs            # 加载同一个卷，可以实现数据共享
      mountPath: /logdata   # 卷 mount 路径
    command: ["/bin/bash"]  # 使用嵌入式脚本统计分析日志
    args:
    - -c
    - |
      while true;do
        awk '{IP[$1]++}END{for(i in IP)print(i,IP[i])}' /logdata/access.log
        sleep 60
      done

[root@master ~]# kubectl apply -f myv1.yaml
pod/web1 created
[root@master ~]# kubectl get pod -o wide
NAME   READY   STATUS    RESTARTS   AGE   IP            NODE
web1   2/2     Running   0          59s   10.244.3.50   node-0003
[root@master ~]# curl -s http://10.244.3.50
Nginx is running !
... ...
[root@master ~]# kubectl logs web1 -c log
10.244.0.0 3
```

#### configMap卷

```shell
# 使用命令创建 configMap
[root@master ~]# kubectl create configmap mycm1 --from-literal=username=admin --from-literal=password=123456
configmap/mycm1 created

# 使用资源文件创建 configMap
[root@master ~]# vim timezone.yaml
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: timezone
data:
  TZ: "Asia/Shanghai"

[root@master ~]# kubectl apply -f timezone.yaml
configmap/timezone created

# 查看 configMap
[root@master ~]# kubectl get configmaps 
NAME               DATA   AGE
kube-root-ca.crt   1      2d5h
mycm1              1      33s
timezone           1      54s
```

##### 修改系统时区

```shell
[root@master ~]# vim myv2.yaml 
---
apiVersion: v1
kind: Pod
metadata:
  name: web2
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: nginx
    image: myos:nginx
    ports:
    - protocol: TCP
      containerPort: 80
    envFrom:             # 引用变量配置
    - configMapRef:      # configmap 资源对象
        name: timezone   # configmap 名称

[root@master ~]# kubectl delete -f myv2.yaml
pod "web2" deleted
[root@master ~]# kubectl apply -f myv2.yaml
pod/web2 created
[root@master ~]# kubectl exec -it web2 -- /bin/bash
[root@web2 html]# echo ${TZ}
Asia/Shanghai
[root@web2 html]# date +%T
# 与我们时间一致
```

##### 创建ConfigMap

```shell
# 拷贝 5/public/info.php 到 master 主机，创建测试页面
[root@master ~]# mkdir webphp
[root@master ~]# echo "Hello Nginx ." >webphp/info.html
[root@master ~]# cp info.php webphp/

# 把目录做 configMap
[root@master ~]# kubectl create configmap website --from-file=webphp
configmap/website created

# 修改 nginx 配置文件，并做成 ConfigMap
[root@master ~]# kubectl cp web2:/usr/local/nginx/conf/nginx.conf ./nginx.conf
[root@master ~]# vim nginx.conf
        location ~ \.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            include        fastcgi.conf;
        } 

[root@master ~]# kubectl create configmap webconf --from-file=nginx.conf
configmap/webconf created
[root@master ~]# kubectl get configmaps 
NAME               DATA   AGE
timezone           1      73m
webconf            1      5s
website            2      4m18s
```

##### nginx解析php

```shell
[root@master ~]# vim myv2.yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: web2
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  volumes:                # 卷配置
  - name: myphp           # 卷名称
    configMap:            # configmap 资源对象
      name: website       # configmap 名称
  - name: webconf         # 卷名称
    configMap:            # configmap 资源对象
      name: webconf       # configmap 名称
  containers:
  - name: nginx
    image: myos:nginx
    volumeMounts:         # mount 卷
    - name: myphp         # 卷名称
      mountPath: /usr/local/nginx/html/myphp        # 路径
    - name: webconf       # 卷名称
      subPath: nginx.conf # 如果是单一文件，需要指定键名称
      mountPath: /usr/local/nginx/conf/nginx.conf   # 路径
    ports:
    - protocol: TCP
      containerPort: 80
    envFrom:
    - configMapRef:
        name: timezone
  - name: php
    image: myos:phpfpm
    volumeMounts:
    - name: myphp
      mountPath: /usr/local/nginx/html/myphp

[root@master ~]# kubectl delete -f myv2.yaml 
pod "web2" deleted
[root@master ~]# kubectl apply -f myv2.yaml 
pod/web2 created
[root@master ~]# kubectl get pods -o wide
NAME   READY   STATUS    RESTARTS   AGE   IP            NODE
web2   2/2     Running   0          7s    10.244.3.13   node-0003
[root@master ~]# curl http://10.244.3.13/myphp/info.php
<pre>
Array
(
    [REMOTE_ADDR] => 10.244.0.0
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /info.php
)
php_host:       web2
1229
```

保护敏感数据（案例2）

```shell
# 修改 nginx.conf 配置文件，启用认证
[root@master ~]# vim nginx.conf 
        location ~ ^/myphp/.+\.php$ {
            root           html;
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            include        fastcgi.conf;
            auth_basic "Website Admin";
            auth_basic_user_file "/usr/local/nginx/conf/webauth";
        }
[root@master ~]# kubectl delete configmaps webconf 
configmap "webconf" deleted
[root@master ~]# kubectl create configmap webconf --from-file=nginx.conf 
configmap/webconf created

# 修改 configmap 之后重建 Pod
[root@master ~]# kubectl delete -f myv2.yaml 
pod "web2" deleted
[root@master ~]# kubectl apply -f myv2.yaml 
pod/web2 created

# 访问验证
[root@master ~]# kubectl get pods -o wide
NAME   READY   STATUS    RESTARTS   AGE    IP            NODE
web2   2/2     Running   0          100s   10.244.2.20   node-0002

[root@master ~]# curl http://10.244.2.20/myphp/info.html
Hello Nginx .

[root@master ~]# curl http://10.244.2.20/myphp/info.php
<html>
<head><title>401 Authorization Required</title></head>
<body>
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx/1.17.6</center>
</body>
</html>
```

#### secret卷

```shell
[root@master ~]# kubectl create secret generic mysecret --from-literal=username=admin --from-literal=password=123456
secret/mysecret created
[root@master ~]# kubectl get secrets 
NAME                  TYPE                                  DATA   AGE
default-token-qw9b9   kubernetes.io/service-account-token   3      5d1h
mysecret              Opaque                                2      54s

# configMap 数据明文存放
[root@master ~]# kubectl get configmaps mycm1 -o yaml
apiVersion: v1
data:
  password: "123456"
  username: admin
kind: ConfigMap
metadata:
  creationTimestamp: "2023-02-01T02:35:54Z"
  name: mycm1
  namespace: default
  resourceVersion: "280963"
  uid: 7ea3cfc4-7930-4d33-b26c-97502710692d

# secret 数据加密存放
[root@master ~]# kubectl get secrets mysecret -o yaml
apiVersion: v1
data:
  password: MTIzNDU2
  username: YWRtaW4=
kind: Secret
metadata:
  creationTimestamp: "2023-02-01T09:56:08Z"
  name: mysecret
  namespace: default
  resourceVersion: "318028"
  uid: 83e3a77d-e8b9-4935-83e7-a3d9bb44f2ce
type: Opaque
```

##### 创建认证文件

为案例2配置用户名密码

```shell
# 生成加密 base64 数据
[root@master ~]# kubectl exec -it web2 -c nginx -- /bin/bash
[root@web2 html]# yum install -y httpd-tools
[root@web2 html]# htpasswd -nbm admin 123456 |base64
YWRtaW46JGFwcjEkdGJqOXJISUckdk9DRFpDaFZJUHl0ZHdGSXl1Qm91MAoK
[root@web2 html]# exit

# 使用 secret 设置密码
[root@master ~]# vim myv2.yaml 
---
kind: Secret
apiVersion: v1
metadata:
  name: myauth
type: Opaque
data:
  webauth: YWRtaW46JGFwcjEkdGJqOXJISUckdk9DRFpDaFZJUHl0ZHdGSXl1Qm91MAoK

---
apiVersion: v1
kind: Pod
metadata:
  name: web2
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  volumes:
  - name: myphp
    configMap:
      name: website
  - name: webconf
    configMap:
      name: webconf
  - name: webauth           # 卷名称
    secret:                 # secret 资源对象
      secretName: myauth    # secret 名称
      items:                # 枚举多个键值
      - key: webauth        # 键值名称
        path: webauth       # 文件名称
        mode: 0644          # 权限
  containers:
  - name: nginx
    image: myos:nginx
    volumeMounts:
    - name: myphp
      mountPath: /usr/local/nginx/html/myphp
    - name: webconf
      subPath: nginx.conf
      mountPath: /usr/local/nginx/conf/nginx.conf
    - name: webauth         # 卷名称
      subPath: webauth      # 键名称
      mountPath: /usr/local/nginx/conf/webauth  # 路径
    ports:
    - protocol: TCP
      containerPort: 80
    envFrom:
    - configMapRef:
        name: timezone
  - name: php
    image: myos:phpfpm
    volumeMounts:
    - name: myphp
      mountPath: /usr/local/nginx/html/myphp

[root@master ~]# kubectl delete pod web2 
pod "web2" deleted
[root@master ~]# kubectl apply -f myv2.yaml 
configmap/timezone created
secret/myauth created
pod/web2 created
[root@master ~]# kubectl get pods -o wide
NAME   READY   STATUS    RESTARTS   AGE   IP            NODE
web2   2/2     Running   0          4s    10.244.3.22   node-0003
[root@master ~]# curl -u admin:123456 http://10.244.3.22/myphp/info.php
<pre>
Array
(
    [REMOTE_ADDR] => 10.244.0.0
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /myphp/info.php
)
php_host:       web2
1229
```

### 持久卷

#### hostPath卷

```shell
[root@master ~]# vim myv3.yaml 
---
apiVersion: v1
kind: Pod
metadata:
  name: web3
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  volumes:                     # 卷配置
  - name: logdata              # 卷名称
    hostPath:                  # hostPath 资源类型
      path: /var/weblog        # 宿主机路径
      type: DirectoryOrCreate  # 目录不存在就创建
  containers:
  - name: nginx
    image: myos:nginx
    ports:
    - protocol: TCP
      containerPort: 80
    volumeMounts:                       # mount 卷
    - name: logdata                     # 卷名称
      mountPath: /usr/local/nginx/logs  # 路径

[root@master ~]# kubectl apply -f myv3.yaml 
pod/web3 created
[root@master ~]# kubectl get pods -o wide
NAME   READY   STATUS    RESTARTS   AGE   IP            NODE
web3   1/1     Running   0          4s    10.244.2.28   node-0002
[root@master ~]# curl http://10.244.2.28/
Nginx is running !

# 删除 Pod ，日志数据也不会丢失
[root@master ~]# kubectl delete -f myv3.yaml 
pod "web3" deleted
[root@node-0002 ~]# cat /var/weblog/access.log 
10.244.0.0 - - [27/Jun/2022:02:00:12 +0000] "GET / HTTP/1.1" 200 19 "-" "curl/7.29.0"
```

#### NFS卷

##### 搭建NFS服务器

```shell
# 搭建 NFS 服务
[root@registry ~]# yum install -y nfs-utils
[root@registry ~]# mkdir -m 0777 /var/webroot
[root@registry ~]# echo "This is NFS server" >/var/webroot/index.html
[root@registry ~]# echo -e "/var/webroot\t*(rw)" >/etc/exports
[root@registry ~]# systemctl enable --now nfs
# 在 master 上验证服务
[root@master ~]# yum install -y nfs-utils
[root@master ~]# showmount -e registry
Export list for registry:
/var/webroot *
# 在所有节点安装 NFS 模块
[root@node-0001 ~]# yum install -y nfs-utils
[root@node-0002 ~]# yum install -y nfs-utils
[root@node-0003 ~]# yum install -y nfs-utils
```

##### Pod调用NFS卷

```shell
[root@master ~]# vim myv3.yaml 
---
apiVersion: v1
kind: Pod
metadata:
  name: web3
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  volumes:
  - name: logdata
    hostPath:
      path: /var/weblog
      type: DirectoryOrCreate
  - name: website              # 卷名称
    nfs:                       # NFS 资源类型
      server: registry         # NFS 服务器地址
      path: /var/webroot       # NFS 共享目录
  containers:
  - name: nginx
    image: myos:nginx
    ports:
    - protocol: TCP
      containerPort: 80
    volumeMounts:
    - name: logdata
      mountPath: /usr/local/nginx/logs
    - name: website                     # 卷名称
      mountPath: /usr/local/nginx/html  # 路径

[root@master ~]# kubectl apply -f myv3.yaml 
pod/web3 created
[root@master ~]# kubectl get pods -o wide
NAME   READY   STATUS    RESTARTS   AGE   IP            NODE
web3   1/1     Running   0          3s    10.244.1.18   node-0001
[root@master ~]# curl http://10.244.1.18/
This is NFS server

# 清理实验 Pod
[root@master ~]# kubectl delete -f myv3.yaml 
pod "web3" deleted
```

### PV/PVC卷

#### 持久卷（PV）

```shell
[root@master ~]# vim pv.yaml
---
kind: PersistentVolume
apiVersion: v1
metadata:
  name: pv-local
spec:
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  capacity:
    storage: 30Gi
  persistentVolumeReclaimPolicy: Retain
  hostPath:
    path: /var/weblog
    type: DirectoryOrCreate

---
kind: PersistentVolume
apiVersion: v1
metadata:                       
  name: pv-nfs
spec:
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
    - ReadOnlyMany
    - ReadWriteMany
  capacity:
    storage: 20Gi
  persistentVolumeReclaimPolicy: Retain
  nfs:
    server: registry
    path: /var/webroot

[root@master ~]# kubectl apply -f pv.yaml 
persistentvolume/pv-local created
persistentvolume/pv-nfs created
[root@master ~]# kubectl get persistentvolume
NAME       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
pv-local   30Gi       RWO            Retain           Available                                   2s
pv-nfs     20Gi       RWO,ROX,RWX    Retain           Available                                   2s
```

#### 持久卷声明（PVC）

```shell
[root@master ~]# vim pvc.yaml
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pvc1
spec:
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 18Gi

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pvc2
spec:
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 15Gi

[root@master ~]# kubectl apply -f pvc.yaml 
persistentvolumeclaim/pvc1 created
persistentvolumeclaim/pvc2 created
[root@master ~]# kubectl get persistentvolumeclaims 
NAME   STATUS   VOLUME     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc1   Bound    pv-local   30Gi       RWO                           8s
pvc2   Bound    pv-nfs     20Gi       RWO,ROX,RWX                   8s
```

#### Pod 调用 PVC

```shell
[root@master ~]# cat myv3.yaml 
---
apiVersion: v1
kind: Pod
metadata:
  name: web3
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  volumes:                   # 卷配置
  - name: logdata            # 卷名称
    persistentVolumeClaim:   # 通过PVC引用存储资源
      claimName: pvc1        # PVC名称
  - name: website            # 卷名称
    persistentVolumeClaim:   # 通过PVC引用存储资源
      claimName: pvc2        # PVC名称
  containers:
  - name: nginx
    image: myos:nginx
    ports:
    - protocol: TCP
      containerPort: 80
    volumeMounts:
    - name: logdata
      mountPath: /usr/local/nginx/logs
    - name: website
      mountPath: /usr/local/nginx/html

[root@master ~]# kubectl apply -f myv3.yaml 
pod/web3 created
[root@master ~]# kubectl get pods -o wide
NAME   READY   STATUS    RESTARTS   AGE   IP            NODE
web3   1/1     Running   0          19s   10.244.1.20   node-0001
[root@master ~]# curl http://10.244.1.20/
This is NFS server

[root@master ~]# kubectl delete -f myv3.yaml 
pod "web3" deleted

[root@node-0001 ~]# cat /var/weblog/access.log 
10.244.0.0 - - [27/Jun/2022:02:00:12 +0000] "GET / HTTP/1.1" 200 19 "-" "curl/7.29.0"
10.244.0.0 - - [27/Jun/2022:02:00:14 +0000] "GET / HTTP/1.1" 200 19 "-" "curl/7.29.0"
```

