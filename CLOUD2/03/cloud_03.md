# kubernetes

[toc]

## Pod调度策略

### 基于节点的调度

```shell
[root@master ~]# kubectl get nodes node-0001
NAME        STATUS   ROLES    AGE     VERSION
node-0001   Ready    <none>   3h      v1.22.5
[root@master ~]# vim myhttpd.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: myhttpd
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  nodeName: node-0001   # 基于节点名称进行调度
  containers:
  - name: apache
    image: myos:httpd
    imagePullPolicy: IfNotPresent
    ports:
    - protocol: TCP
      containerPort: 80

[root@master ~]# kubectl apply -f myhttpd.yaml  
pod/myhttpd created
[root@master ~]# kubectl get pods -o wide
NAME      READY   STATUS    RESTARTS   AGE   IP           NODE
myhttpd   1/1     Running   0          3s    10.244.1.3   node-0001
```

### 标签管理

```shell
# 查询资源对象标签
[root@master ~]# kubectl get pods --show-labels 
NAME      READY   STATUS    RESTARTS   AGE     LABELS
myhttpd   1/1     Running   0          2m34s   <none>
[root@master ~]# kubectl get namespaces --show-labels 
NAME              STATUS   AGE     LABELS
default           Active   3h44m   kubernetes.io/metadata.name=default
kube-node-lease   Active   3h44m   kubernetes.io/metadata.name=kube-node-lease
kube-public       Active   3h44m   kubernetes.io/metadata.name=kube-public
kube-system       Active   3h44m   kubernetes.io/metadata.name=kube-system
[root@master ~]# kubectl get nodes --show-labels 
NAME        STATUS   ROLES                  AGE     VERSION   LABELS
master      Ready    control-plane,master   3h44m   v1.22.5   kubernetes.io/hostname=master ... ...
node-0001   Ready    <none>                 3h38m   v1.22.5   kubernetes.io/hostname=node-0001 ... ...
node-0002   Ready    <none>                 3h38m   v1.22.5   kubernetes.io/hostname=node-0002 ... ...
node-0003   Ready    <none>                 3h38m   v1.22.5   kubernetes.io/hostname=node-0003 ... ...

# 使用标签过滤资源对象
[root@master ~]# kubectl get nodes -l kubernetes.io/hostname=master
NAME     STATUS   ROLES                     AGE     VERSION
master   Ready    control-plane,master      3h38m   v1.22.5

# 为资源对象添加标签
[root@master ~]# kubectl get pods --show-labels 
NAME      READY   STATUS    RESTARTS   AGE   LABELS
myhttpd   1/1     Running   0          14m   <none>
[root@master ~]# kubectl label pod myhttpd app=apache
pod/myhttpd labeled
[root@master ~]# kubectl get pods --show-labels 
NAME      READY   STATUS    RESTARTS   AGE   LABELS
myhttpd   1/1     Running   0          14m   app=apache

# 删除标签
[root@master ~]# kubectl label pod myhttpd app-
pod/myhttpd labeled
[root@master ~]# kubectl get pods --show-labels 
NAME      READY   STATUS    RESTARTS   AGE   LABELS
myhttpd   1/1     Running   0          14m   <none>
```

资源文件中配置标签

```shell
[root@master ~]# vim myhttpd.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: myhttpd
  labels:               # 声明标签
    app: apache         # 标签键值对
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: apache
    image: myos:httpd
    imagePullPolicy: IfNotPresent
    ports:
    - protocol: TCP
      containerPort: 80

ot@master ~]# kubectl delete -f myhttpd.yaml
pod "myhttpd" deleted
[root@master ~]# kubectl apply -f myhttpd.yaml  
pod/myhttpd created
[root@master ~]# kubectl get pods --show-labels 
NAME      READY   STATUS    RESTARTS   AGE   LABELS
myhttpd   1/1     Running   0          14m   app=apache
```

### 基于标签的调度

```shell
[root@master ~]# kubectl get nodes node-0002 --show-labels 
NAME        STATUS   ROLES     AGE     VERSION   LABELS
node-0002   Ready    <none>    3h38m   v1.22.5   kubernetes.io/hostname=node-0002 ... ...

[root@master ~]# vim myhttpd.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: myhttpd
  labels:
    app: apache
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  nodeSelector:                        # 基于节点标签进行调度
    kubernetes.io/hostname: node-0002  # 标签
  containers:
  - name: apache
    image: myos:httpd
    imagePullPolicy: IfNotPresent
    ports:
    - protocol: TCP
      containerPort: 80

[root@master ~]# kubectl delete -f myhttpd.yaml 
pod "myhttpd" deleted
[root@master ~]# kubectl apply -f myhttpd.yaml  
pod/myhttpd created
[root@master ~]# kubectl get pods -l app=apache -o wide
NAME      READY   STATUS    RESTARTS   AGE   IP           NODE
myhttpd   1/1     Running   0          9s    10.244.2.4   node-0002
```

### 容器调度（案例2）

```shell
[root@master ~]# kubectl label nodes node-0002 node-0003 disktype=ssd
node/node-0002 labeled
node/node-0003 labeled
[root@master ~]# vim myhttpd.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: myhttpd
  labels:
    app: apache
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  nodeSelector:
    disktype: ssd
  containers:
  - name: apache
    image: myos:httpd
    imagePullPolicy: IfNotPresent
    ports:
    - protocol: TCP
      containerPort: 80

[root@master ~]# for i in web{1..5};do sed "s,myhttpd,${i}," myhttpd.yaml ; done |kubectl apply -f -
pod/web1 created
pod/web2 created
pod/web3 created
pod/web4 created
pod/web5 created
[root@master ~]# kubectl get pods -o wide
NAME      READY   STATUS    RESTARTS   AGE   IP            NODE
myhttpd   1/1     Running   0          29m   10.244.3.80   node-0003
web1      1/1     Running   0          10s   10.244.2.60   node-0002
web2      1/1     Running   0          10s   10.244.3.82   node-0003
web3      1/1     Running   0          10s   10.244.2.61   node-0002
web4      1/1     Running   0          10s   10.244.2.62   node-0002
web5      1/1     Running   0          10s   10.244.3.81   node-0003

# 清理实验配置
[root@master ~]# kubectl delete -f myhttpd.yaml 
pod "myhttpd" deleted
[root@master ~]# kubectl delete pod web{1..5}
pod "web1" deleted
pod "web2" deleted
pod "web3" deleted
pod "web4" deleted
pod "web5" deleted
[root@master ~]# kubectl label nodes node-0002 node-0003 disktype-
node/node-0002 labeled
node/node-0003 labeled
```

## Pod资源配额

### 最小资源配额

内存需求配额

```shell
[root@master ~]# vim minpod.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: minpod
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  nodeSelector:
    kubernetes.io/hostname: node-0003
  containers:
  - name: linux
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]
    resources:               # 设置配额
      requests:              # 最小配额
        memory: "1200Mi"     # 内存配额

[root@master ~]# sed 's,minpod,app1,' minpod.yaml |kubectl apply -f -
pod/app1 created
[root@master ~]# sed 's,minpod,app2,' minpod.yaml |kubectl apply -f -
pod/app2 created
[root@master ~]# sed 's,minpod,app3,' minpod.yaml |kubectl apply -f -
pod/app3 created
[root@master ~]# kubectl get pods
NAME   READY   STATUS    RESTARTS   AGE
app1   1/1     Running   0          16s
app2   1/1     Running   0          7s
app3   1/1     Running   0          1s
[root@master ~]# sed 's,minpod,app4,' minpod.yaml |kubectl apply -f -
pod/app4 created
[root@master ~]# kubectl get pods
NAME   READY   STATUS    RESTARTS   AGE
app1   1/1     Running   0          24s
app2   1/1     Running   0          15s
app3   1/1     Running   0          9s
app4   0/1     Pending   0          2s
[root@master ~]# kubectl delete pod app{1..4}
pod "app1" deleted
pod "app2" deleted
pod "app3" deleted
pod "app4" deleted
```

计算资源需求配额

```shell
[root@master ~]# vim minpod.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: minpod
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  nodeSelector:
    kubernetes.io/hostname: node-0003
  containers:
  - name: linux
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]
    resources:
      requests:
        cpu: "800m"          # 计算资源需求
        memory: "1200Mi"

[root@master ~]# sed 's,minpod,app1,' minpod.yaml |kubectl apply -f -
pod/app1 created
[root@master ~]# sed 's,minpod,app2,' minpod.yaml |kubectl apply -f -
pod/app2 created
[root@master ~]# sed 's,minpod,app3,' minpod.yaml |kubectl apply -f -
pod/app3 created
[root@master ~]# kubectl get pods
NAME   READY   STATUS    RESTARTS   AGE
app1   1/1     Running   0          9s
app2   1/1     Running   0          6s
app3   0/1     Pending   0          3s
[root@master ~]# kubectl delete pod app{1..3}
pod "app1" deleted
pod "app2" deleted
pod "app3" deleted
```

### 最大资源配额

拷贝 5/public/memtest.py 到 master 主机

```shell
[root@master ~]# vim maxpod.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: maxpod
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: linux
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]

# 不设置配额内存与计算资源都没有限制
[root@master ~]# kubectl cp memtest.py maxpod:/usr/bin/
[root@master ~]# kubectl exec -it maxpod -- /bin/bash
[root@maxpod /]# memtest.py 2500
use memory success
press any key to exit : 
[root@maxpod /]# ps aux
USER     PID   %CPU   %MEM    VSZ    RSS  TTY  STAT  START   TIME  COMMAND
root       1   99.9    0.0    9924   128   ?   Rs    07:45   3:52  awk BEGIN{while(1){}}
```

添加资源配额

```shell
[root@master ~]# vim maxpod.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: maxpod
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: linux
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]
    resources:
      limits:
        cpu: "800m"
        memory: "2000Mi"

[root@master ~]# kubectl delete -f maxpod.yaml 
pod "maxpod" deleted
[root@master ~]# kubectl apply -f maxpod.yaml 
pod/maxpod created
[root@master ~]# kubectl cp memtest.py maxpod:/usr/bin/
[root@master ~]# kubectl exec -it maxpod -- /bin/bash
[root@maxpod /]# memtest.py 2500
Killed
[root@maxpod /]# memtest.py 1500
use memory success
press any key to exit :
[root@maxpod /]# ps aux
USER     PID   %CPU   %MEM    VSZ    RSS  TTY  STAT  START   TIME  COMMAND
root       1   79.8    0.0    9924   484   ?   Rs    07:52   1:10  awk BEGIN{while(1){}}

[root@master ~]# kubectl delete -f maxpod.yaml 
pod "maxpod" deleted
```

## 全局资源配额

### 默认配额策略

```shell
# 创建名称空间
[root@master ~]# kubectl create namespace myns
namespace/myns created
# 设置默认配额
[root@master ~]# vim mynslimit.yaml
---
apiVersion: v1
kind: LimitRange
metadata:
  name: mylimit 
  namespace: myns       
spec:
  limits:               
  - type: Container     
    default:            
      cpu: 300m 
      memory: 500Mi     
    defaultRequest:
      cpu: 8m  
      memory: 8Mi 

[root@master ~]# kubectl -n myns apply -f mynslimit.yaml
limitrange/mylimit created 
```

验证默认资源配额

```shell
# 删除配额策略，创建容器
[root@master ~]# vim maxpod.yaml
---
kind: Pod
apiVersion: v1
metadata:
  name: maxpod
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: linux
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]

[root@master ~]# kubectl -n myns apply -f maxpod.yaml
pod/maxpod created
[root@master ~]# kubectl -n myns cp memtest.py maxpod:/usr/bin/
[root@master ~]# kubectl -n myns exec -it maxpod -- /bin/bash
[root@maxpod /]# memtest.py 500
Killed
[root@maxpod /]# memtest.py 300
use memory success
press any key to exit : 
[root@maxpod /]# ps aux
USER     PID   %CPU   %MEM    VSZ    RSS  TTY  STAT  START   TIME  COMMAND
root       1   28.9    0.0    9924   720   ?   Rs    08:09   0:09  awk BEGIN{while(1){}}

[root@master ~]# kubectl -n myns describe pod maxpod
... ...
    Limits:
      cpu:     300m
      memory:  500Mi
    Requests:
      cpu:     10m
      memory:  8Mi
... ...
```

用户自定义资源配额

```shell
[root@master ~]# vim maxpod.yaml
---
kind: Pod
apiVersion: v1
metadata:
  name: maxpod
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: linux
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]
    resources:
      limits:
        cpu: "1000m"
        memory: "2000Mi"

[root@master ~]# kubectl -n myns delete -f maxpod.yaml 
pod "maxpod" deleted
[root@master ~]# kubectl -n myns apply -f maxpod.yaml
pod/maxpod created

[root@master ~]# kubectl -n myns exec -it maxpod -- /bin/bash
[root@maxpod /]# ps aux
USER     PID   %CPU   %MEM    VSZ    RSS  TTY  STAT  START   TIME  COMMAND
root       1   99.9    0.0    9924   720   ?   Rs    08:09   0:09  awk BEGIN{while(1){}}

[root@master ~]# kubectl -n myns describe pod maxpod
... ...
    Limits:
      cpu:     1000m
      memory:  2000Mi
    Requests:
      cpu:     10m
      memory:  8Mi
... ...
```

### 资源配额范围

```shell
[root@master ~]# vim mynslimit.yaml 
---
apiVersion: v1
kind: LimitRange
metadata:
  name: mylimit
  namespace: myns
spec:
  limits:               
  - type: Container     
    default:            
      cpu: 300m 
      memory: 500Mi     
    defaultRequest:
      cpu: 8m  
      memory: 8Mi 
    max:
      cpu: "800m"
      memory: "1000Mi"
    min:
      cpu: "2m"
      memory: "8Mi"

[root@master ~]# kubectl -n myns apply -f mynslimit.yaml 
limitrange/mylimit configured

[root@master ~]# kubectl -n myns delete -f maxpod.yaml 
pod "maxpod" deleted
[root@master ~]# kubectl -n myns apply -f maxpod.yaml 
Error from server (Forbidden): error when creating "maxpod.yaml": pods "maxpod" is forbidden: [maximum cpu usage per Container is 800m, but limit is 1, maximum memory usage per Container is 1000Mi, but limit is 2000Mi]
```

多容器资源配额

```shell
[root@master ~]# vim maxpod.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: maxpod
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: c1
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]
    resources:
      limits:
        cpu: "800m"
        memory: "1000Mi"
  - name: c2
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]
    resources:
      limits:
        cpu: "800m"
        memory: "1000Mi"

[root@master ~]# kubectl -n myns apply -f maxpod.yaml 
pod/maxpod created
[root@master ~]# kubectl -n myns get pods -o wide
NAME     READY   STATUS    RESTARTS   AGE    IP            NODE
maxpod   2/2     Running   0          107s   10.244.2.65   node-0002
#----------------------------------------------------------------------
# 在节点上查看资源占用情况
[root@node-0002 ~]# ps aux |grep awk
root     20369 80.1  0.0   9924   720 ?     Rs   16:23   2:38 awk BEGIN{while(1){}}
root     20405 79.9  0.0   9924   720 ?     Rs   16:23   2:38 awk BEGIN{while(1){}}
... ...
```

### 基于 Pod 的资源配额

```shell
[root@master ~]# vim mynslimit.yaml 
---
apiVersion: v1
kind: LimitRange
metadata:
  name: mylimit
  namespace: myns
spec:
  limits:               
  - type: Container     
    default:            
      cpu: 300m 
      memory: 500Mi     
    defaultRequest:
      cpu: 8m  
      memory: 8Mi 
    max:
      cpu: "800m"
      memory: "1000Mi"
    min:
      cpu: "2m"
      memory: "8Mi"
  - type: Pod
    max:
      cpu: "1200m"
      memory: "1200Mi"
    min:
      cpu: "2m"
      memory: "8Mi"

[root@master ~]# kubectl -n myns apply -f mynslimit.yaml
limitrange/mylimit configured

[root@master ~]# kubectl -n myns delete -f maxpod.yaml 
pod "maxpod" deleted
[root@master ~]# kubectl -n myns apply -f maxpod.yaml 
Error from server (Forbidden): error when creating "maxpod.yaml": pods "maxpod" is forbidden: [maximum cpu usage per Pod is 1200m, but limit is 1600m, maximum memory usage per Pod is 1200Mi, but limit is 2097152k]
```

## 全局 quota 配额

多个 Pod 消耗资源

```shell
[root@master ~]# vim maxpod.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: maxpod
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: c1
    image: myos:v2009
    command: ["awk", "BEGIN{while(1){}}"]
    resources:
      requests:
        cpu: "8m"
        memory: "8Mi"
      limits:
        cpu: "600m"
        memory: "1000Mi"

# 创建太多Pod，资源也会耗尽
[root@master ~]# for i in app{1..9};do sed "s,maxpod,${i}," maxpod.yaml ;done |kubectl -n myns apply -f -
pod/app1 created
pod/app2 created
pod/app3 created
pod/app4 created
pod/app5 created
pod/app6 created
pod/app7 created
pod/app8 created
pod/app9 created

#----------------------------------------------------------------------
# 在计算节点上查看资源占用情况
[root@node-0001 ~]# ps aux |grep awk
root     26768 60.7  0.0   9924   716 ?     Rs   16:35   0:29 awk BEGIN{while(1){}}
root     26842 60.2  0.0   9924   716 ?     Rs   16:35   0:29 awk BEGIN{while(1){}}
root     26870 60.1  0.0   9924   716 ?     Rs   16:35   0:29 awk BEGIN{while(1){}}
```

### 基于总数量配额

```shell
[root@master ~]# vim mynsquota.yaml
---
apiVersion: v1
kind: ResourceQuota
metadata:
  name: myquota
  namespace: myns
spec:
  hard:
    requests.cpu: "1000m"
    requests.memory: "2000Mi"
    limits.cpu: "5000m"
    limits.memory: "8Gi"
    pods: "3"

[root@master ~]# kubectl -n myns apply -f mynsquota.yaml 
resourcequota/myquota created
```

验证 quota 配额

```shell
[root@master ~]# kubectl -n myns delete pod app{1..9}
pod "app1" deleted
pod "app2" deleted
pod "app3" deleted
pod "app4" deleted
pod "app5" deleted
pod "app6" deleted
pod "app7" deleted
pod "app8" deleted
pod "app9" deleted

[root@master ~]# sed 's,maxpod,app1,' maxpod.yaml |kubectl -n myns apply -f -
pod/app1 created
[root@master ~]# sed 's,maxpod,app2,' maxpod.yaml |kubectl -n myns apply -f -
pod/app2 created
[root@master ~]# sed 's,maxpod,app3,' maxpod.yaml |kubectl -n myns apply -f -
pod/app3 created
[root@master ~]# sed 's,maxpod,app4,' maxpod.yaml |kubectl -n myns apply -f -
Error from server (Forbidden): error when creating "STDIN": pods "app4" is forbidden: exceeded quota: myquota, requested: pods=1, used: pods=3, limited: pods=3

# 删除实验 Pod 与限额规则
[root@master ~]# kubectl -n myns delete pod app{1..3}
pod "app1" deleted
pod "app2" deleted
pod "app3" deleted
[root@master ~]# kubectl -n myns delete -f mynslimit.yaml -f mynsquota.yaml
limitrange "mylimit" deleted
resourcequota "myquota" deleted
[root@master ~]# kubectl delete namespace myns
namespace "myns" deleted
```

## 资源监控组件

### 配置授权令牌

```shell
[root@master ~]# echo 'serverTLSBootstrap: true' >>/var/lib/kubelet/config.yaml
[root@master ~]# systemctl restart kubelet
[root@master ~]# kubectl get certificatesigningrequests.certificates.k8s.io 
NAME        AGE   SIGNERNAME                      REQUESTOR            REQUESTEDDURATION   CONDITION
csr-6zhbx   14s   kubernetes.io/kubelet-serving   system:node:master   <none>              Pending
[root@master ~]# kubectl certificate approve csr-6zhbx
certificatesigningrequest.certificates.k8s.io/csr-6zhbx approved
[root@master ~]# kubectl get certificatesigningrequests.certificates.k8s.io 
NAME        AGE   SIGNERNAME                      REQUESTOR            REQUESTEDDURATION   CONDITION
csr-6zhbx   28s   kubernetes.io/kubelet-serving   system:node:master   <none>              Approved,Issued
```

### 安装 metrics 插件

第五阶段/kubernetes/plugins/metrics

```shell
[root@master metrics]# docker load -i metrics-server.tar.xz
[root@master metrics]# docker tag k8s.gcr.io/metrics-server/metrics-server:v0.5.2 registry:5000/plugins/metrics-server:v0.5.2
[root@master metrics]# docker push registry:5000/plugins/metrics-server:v0.5.2
[root@master metrics]# docker rmi k8s.gcr.io/metrics-server/metrics-server:v0.5.2 registry:5000/plugins/metrics-server:v0.5.2
[root@master metrics]# sed -ri 's,^(\s+image: ).+/(.+),\1registry:5000/plugins/\2,' components.yaml
138:     image: registry:5000/plugins/metrics-server:v0.5.2
[root@master metrics]# kubectl apply -f components.yaml
[root@master metrics]# kubectl -n kube-system get pods -l k8s-app=metrics-server
NAME                             READY   STATUS    RESTARTS   AGE
metrics-server-ddb449849-c6lkc   1/1     Running   0          64s
[root@master metrics]# kubectl top nodes
NAME        CPU(cores)   CPU%        MEMORY(bytes)     MEMORY%     
master      99m          4%          1005Mi            27%         
node-0001   <unknown>    <unknown>    <unknown>       <unknown>
node-0002   <unknown>    <unknown>    <unknown>       <unknown>
node-0003   <unknown>    <unknown>    <unknown>       <unknown>
```

### 为计算节点签发证书

```shell
#--------------- 签发 node-0001 证书 -----------------
[root@node-0001 ~]# echo 'serverTLSBootstrap: true' >>/var/lib/kubelet/config.yaml
[root@node-0001 ~]# systemctl restart kubelet
# 在 master 上签发证书
[root@master ~]# kubectl get certificatesigningrequests.certificates.k8s.io 
NAME        AGE   SIGNERNAME                      REQUESTOR        REQUESTEDDURATION   CONDITION
csr-6zhbx   14m   kubernetes.io/kubelet-serving   master           <none>              Approved,Issued
csr-p97jk   28s   kubernetes.io/kubelet-serving   node-0001        <none>              Pending
[root@master ~]# kubectl certificate approve csr-p97jk
certificatesigningrequest.certificates.k8s.io/csr-p97jk approved
[root@master ~]# kubectl get certificatesigningrequests.certificates.k8s.io 
NAME        AGE   SIGNERNAME                      REQUESTOR        REQUESTEDDURATION   CONDITION
csr-6zhbx   14m   kubernetes.io/kubelet-serving   master           <none>              Approved,Issued
csr-p97jk   28s   kubernetes.io/kubelet-serving   node-0001        <none>              Approved,Issued

#--------------- 签发 node-0002 证书 -----------------
[root@node-0002 ~]# echo 'serverTLSBootstrap: true' >>/var/lib/kubelet/config.yaml
[root@node-0002 ~]# systemctl restart kubelet
# 在 master 上签发证书
[root@master ~]# kubectl get certificatesigningrequests.certificates.k8s.io 
NAME        AGE   SIGNERNAME                      REQUESTOR        REQUESTEDDURATION   CONDITION
csr-6zhbx   14m   kubernetes.io/kubelet-serving   master           <none>              Approved,Issued
csr-p97jk   28s   kubernetes.io/kubelet-serving   node-0001        <none>              Approved,Issued
csr-fmc2d   21s   kubernetes.io/kubelet-serving   node-0002        <none>              Pending
[root@master ~]# kubectl certificate approve csr-fmc2d
certificatesigningrequest.certificates.k8s.io/csr-fmc2d approved
[root@master ~]# kubectl get certificatesigningrequests.certificates.k8s.io 
NAME        AGE   SIGNERNAME                      REQUESTOR        REQUESTEDDURATION   CONDITION
csr-6zhbx   14m   kubernetes.io/kubelet-serving   master           <none>              Approved,Issued
csr-p97jk   28s   kubernetes.io/kubelet-serving   node-0001        <none>              Approved,Issued
csr-fmc2d   21s   kubernetes.io/kubelet-serving   node-0002        <none>              Approved,Issued

#--------------- 签发 node-0003 证书 -----------------
[root@node-0003 ~]# echo 'serverTLSBootstrap: true' >>/var/lib/kubelet/config.yaml
[root@node-0003 ~]# systemctl restart kubelet
# 在 master 上签发证书
[root@master ~]# kubectl get certificatesigningrequests.certificates.k8s.io 
NAME        AGE   SIGNERNAME                      REQUESTOR        REQUESTEDDURATION   CONDITION
csr-6zhbx   14m   kubernetes.io/kubelet-serving   master           <none>              Approved,Issued
csr-p97jk   28s   kubernetes.io/kubelet-serving   node-0001        <none>              Approved,Issued
csr-fmc2d   21s   kubernetes.io/kubelet-serving   node-0002        <none>              Approved,Issued
csr-frs9k   15s   kubernetes.io/kubelet-serving   node-0003        <none>              Pending
[root@master ~]# kubectl certificate approve csr-frs9k
certificatesigningrequest.certificates.k8s.io/csr-frs9k approved
[root@master ~]# kubectl get certificatesigningrequests.certificates.k8s.io 
NAME        AGE   SIGNERNAME                      REQUESTOR        REQUESTEDDURATION   CONDITION
csr-6zhbx   14m   kubernetes.io/kubelet-serving   master           <none>              Approved,Issued
csr-p97jk   28s   kubernetes.io/kubelet-serving   node-0001        <none>              Approved,Issued
csr-fmc2d   21s   kubernetes.io/kubelet-serving   node-0002        <none>              Approved,Issued
csr-frs9k   15s   kubernetes.io/kubelet-serving   node-0003        <none>              Approved,Issued
```

验证 metrics 插件

```shell
[root@master ~]# kubectl top nodes
NAME        CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
master      92m          4%     995Mi           26%       
node-0001   28m          1%     327Mi           8%        
node-0002   28m          1%     365Mi           9%        
node-0003   32m          1%     338Mi           9% 
[root@master ~]# kubectl apply -f maxpod.yaml 
pod/maxpod created
[root@master ~]# kubectl cp memtest.py maxpod:/usr/bin/
[root@master ~]# kubectl exec -it maxpod -- memtest.py 800
use memory success
press any key to exit : 

#-------------------------------------------------------
# 在另一个终端使用 top 查看 Pod 资源使用情况
[root@master ~]# kubectl top pods
NAME     CPU(cores)   MEMORY(bytes)   
maxpod   600m         804Mi           
[root@master ~]# kubectl top nodes
NAME        CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
master      88m          4%     1380Mi          37%       
node-0001   26m          1%     55Mi            1%       
node-0002   624m         31%    857Mi           23%       
node-0003   32m          1%     47Mi            1%  

# 完成实验以后删除 Pod
[root@master ~]# kubectl delete -f maxpod.yaml 
pod "maxpod" deleted
```

