# kubernetes

[toc]

## Pod 创建过程

```mermaid
sequenceDiagram
  User ->> + API Server: create pod
  API Server ->> + etcd: write
  etcd -->> - API Server: Confirm
  API Server -->> User: Confirm
  API Server ->> + Scheduler: watch(new pod)
  Scheduler ->> - API Server: bind pod
  API Server ->> + etcd: write
  etcd -->> - API Server: Confirm
  API Server -->> Scheduler: Confirm
  API Server ->> + kubelet: watch(bound pod)
  kubelet ->> + Runtime: Runtime-shim
  Runtime -->> - kubelet: Confirm
  kubelet ->> - API Server: update pod status
  API Server ->> + etcd: write
  etcd -->> - API Server: Confirm
  API Server -->> - kubelet: Confirm
```

## Pod 生命周期

```mermaid
gantt
  title Pod生命周期
  dateFormat mm
  section init
    Init1   :00, 01
    Init2   :01, 02
    Init3   :02, 03
  section main
    post start     : 03, 04
    liveness probe : 04, 07
    readiness probe: 04, 07
    pre stop       : 07, 08
    main Container : crit, active, 03, 08
```

## kubectl 管理命令

### 管理命令（一）

| **子命令**    | **说明**                            | **备注** |
| ------------- | ----------------------------------- | -------- |
| help          | 用于查看命令及子命令的帮助信息      |          |
| cluster-info  | 显示集群的相关配置信息              |          |
| version       | 查看服务器及客户端的版本信息        |          |
| api-resources | 查看当前服务器上所有的资源对象      |          |
| api-versions  | 查看当前服务器上所有资源对象的版本  |          |
| config        | 管理当前节点上kubeconfig 的认证信息 |          |

#### 命令示例

```shell
# 查看帮助命令信息
[root@master ~]# kubectl help version
Print the client and server version information for the current context.

Examples:
  # Print the client and server versions for the current context
  kubectl version
... ...

# 查看集群状态信息
[root@master ~]# kubectl cluster-info 
Kubernetes control plane is running at https://192.168.1.50:6443
CoreDNS is running at https://192.168.1.50:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
... ...

# 查看服务端与客户端版本信息
[root@master ~]# kubectl version 
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.5", GitCommit:"5c99e2ac2ff9a3c549d9ca665e7bc05a3e18f07e", GitTreeState:"clean", BuildDate:"2021-12-16T08:38:33Z", GoVersion:"go1.16.12", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.5", GitCommit:"5c99e2ac2ff9a3c549d9ca665e7bc05a3e18f07e", GitTreeState:"clean", BuildDate:"2021-12-16T08:32:32Z", GoVersion:"go1.16.12", Compiler:"gc", Platform:"linux/amd64"}

# 查看资源对象类型
[root@master ~]# kubectl api-resources 
NAME             SHORTNAMES     APIVERSION      NAMESPACED      KIND
bindings                        v1              true            Binding
endpoints        ep             v1              true            Endpoints
events           ev             v1              true            Event
... ...

# 查看资源对象版本
[root@master ~]# kubectl api-versions 
admissionregistration.k8s.io/v1
apiextensions.k8s.io/v1
apiregistration.k8s.io/v1
apps/v1
... ...

# 查看当前认证使用的用户及证书
[root@master ~]# kubectl config get-contexts 
CURRENT   NAME                          CLUSTER      AUTHINFO           NAMESPACE
*         kubernetes-admin@kubernetes   kubernetes   kubernetes-admin   
```

为registry主机配置管理授权

```shell
[root@registry ~]# vim /etc/hosts
192.168.1.30    registry
192.168.1.50    master
192.168.1.51    node-0001
192.168.1.52    node-0002
192.168.1.53    node-0003
[root@registry ~]# yum install -y kubectl
[root@registry ~]# source <(kubectl completion bash|tee /etc/bash_completion.d/kubectl)
[root@registry ~]# mkdir -p $HOME/.kube
[root@registry ~]# sudo rsync -av master:/etc/kubernetes/admin.conf $HOME/.kube/config
[root@registry ~]# sudo chown $(id -u):$(id -g) $HOME/.kube/config
[root@registry ~]# kubectl get nodes
NAME        STATUS   ROLES                  AGE   VERSION
master      Ready    control-plane,master   23h   v1.22.5
node-0001   Ready    <none>                 22h   v1.22.5
node-0002   Ready    <none>                 22h   v1.22.5
node-0003   Ready    <none>                 22h   v1.22.5
```

### 导入自定义镜像

```shell
# 拷贝 public/myos.tar.xz 目录到 master 云主机上
[root@ecs-proxy ~]# rsync -av public/myos.tar.xz 192.168.1.50:./plugins/
```

```shell
# 导入镜像
[root@master ~]# docker load -i plugins/myos.tar.xz
[root@master ~]# docker images|while read i t _;do
    [[ "${t}" == "TAG" ]] && continue
    [[ "${i}" =~ ^"registry:5000/".+ ]] && continue
    docker tag ${i}:${t} registry:5000/library/${i##*/}:${t}
    docker push registry:5000/library/${i##*/}:${t}
    docker rmi ${i}:${t} registry:5000/library/${i##*/}:${t}
done
# 验证结果
[root@master ~]# curl -s http://registry:5000/v2/_catalog|python -m json.tool
{
    "repositories": [
        "k8s/coredns",
        "k8s/etcd",
        "k8s/kube-apiserver",
        "k8s/kube-controller-manager",
        "k8s/kube-proxy",
        "k8s/kube-scheduler",
        "k8s/pause",
        "library/centos",
        "library/myos",
        "plugins/mirrored-flannelcni-flannel",
        "plugins/mirrored-flannelcni-flannel-cni-plugin"
    ]
}
[root@master ~]# curl -s http://registry:5000/v2/library/myos/tags/list |python -m json.tool
{
    "name": "library/myos",
    "tags": [
        "latest",
        "httpd",
        "phpfpm",
        "nginx",
        "v2009"
    ]
}
```

### 管理命令（二）

| **子命令** | **说明**               | **备注**               |
| ---------- | ---------------------- | ---------------------- |
| run        | 创建Pod资源对象        | 一般用来创建 Pod 模板  |
| get        | 查看资源对象的状态信息 | 可选参数:  -o 显示格式 |
| describe   | 查询资源对象的属性信息 |                        |
| logs       | 查看容器的报错信息     | 可选参数:  -c 容器名称 |

#### 命令示例

```shell
# 创建 Pod
[root@master ~]# kubectl run myweb --image=myos:httpd
pod/myweb created
[root@master ~]# kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
myweb   1/1     Running   0          26s
# 创建交互式 Pod
[root@master ~]# kubectl run mypod -it --image=myos:v2009
If you don't see a command prompt, try pressing enter.
[root@mypod /]# : 这里已经进入Pod了

# 查询 Pod 信息
[root@master ~]# kubectl get pods
NAME    READY   STATUS    RESTARTS     AGE
mypod   1/1     Running   1 (3s ago)   18s
myweb   1/1     Running   0            68m
[root@master ~]# kubectl get pods -o name
pod/mypod
pod/myweb
[root@master ~]# kubectl get pods -o wide
NAME    READY   STATUS    RESTARTS      AGE   IP           NODE
mypod   1/1     Running   1 (39s ago)   54s   10.244.1.2   node-0001
myweb   1/1     Running   0             69m   10.244.2.2   node-0002

# 查询 pod 的属性信息
[root@master ~]# kubectl describe pod myweb
Name:         myweb
... ...
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  18s   default-scheduler  Successfully assigned default/myweb to node-0002
  Normal  Pulling    17s   kubelet            Pulling image "myos:httpd"
  Normal  Pulled     17s   kubelet            Successfully pulled image "myos:httpd" in 153.394005ms
  Normal  Created    17s   kubelet            Created container linux
  Normal  Started    17s   kubelet            Started container linux

# 查询 pod 的日志信息
[root@master ~]# kubectl logs myweb
[root@master ~]# 
```

查询其他名称空间的 Pod 信息

```shell
# 查询节点信息
[root@master ~]# kubectl get namespaces 
NAME              STATUS   AGE
default           Active   44m
kube-node-lease   Active   44m
kube-public       Active   44m
kube-system       Active   44m

# 查询 kube-system 名称空间下 Pod 信息
[root@master ~]# kubectl -n kube-system get pods
NAME                             READY   STATUS    RESTARTS   AGE
coredns-54b6487f4d-t7f9m         1/1     Running   0          120m
coredns-54b6487f4d-v2zbg         1/1     Running   0          120m
etcd-master                      1/1     Running   0          120m
kube-apiserver-master            1/1     Running   0          120m
kube-controller-manager-master   1/1     Running   0          120m
kube-flannel-ds-8x4hq            1/1     Running   0          111m
kube-flannel-ds-c5rkv            1/1     Running   0          111m
kube-flannel-ds-sk2gj            1/1     Running   0          111m
kube-flannel-ds-v26sx            1/1     Running   0          111m
kube-proxy-6gprw                 1/1     Running   0          115m
kube-proxy-6tfn8                 1/1     Running   0          115m
kube-proxy-9t5ln                 1/1     Running   0          115m
kube-proxy-f956k                 1/1     Running   0          120m
kube-scheduler-master            1/1     Running   0          120m
```

#### 排错练习(案例2)

```shell
[root@master config]# vim pod1.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: pod1
spec:
  containers:
  - name: linux
    image: myOs:httpd

[root@master config]# kubectl apply -f pod1.yaml
pod/pod1 created
```

### 管理命令（三）

| **子命令** | **说明**                         | **备注**               |
| ---------- | -------------------------------- | ---------------------- |
| exec       | 在某一个容器内执行特定的命令     | 可选参数:  -c 容器名称 |
| cp         | 在容器和宿主机之间拷贝文件或目录 | 可选参数:  -c 容器名称 |
| delete     | 删除资源对象                     | 可选参数:  -f 文件名称 |
| create     | 创建资源对象                     | 必选参数:  -f 文件名称 |
| apply      | （创建/更新）资源对象            | 必选参数:  -f 文件名称 |

#### 命令示例

```shell
# 使用资源文件创建/更新Pod
[root@master ~]# vim mypod.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: mypod
spec:
  containers:
  - name: linux
    image: myos:latest
    stdin: true
    tty: true

[root@master ~]# kubectl apply -f mypod.yaml 
Warning: resource pods/mypod is missing the kubectl.kubernetes.io/last-applied-configuration annotation which is required by kubectl apply. kubectl apply should only be used on resources created declaratively by either kubectl create --save-config or kubectl apply. The missing annotation will be patched automatically.
The Pod "mypod" is invalid: spec.containers: Forbidden: pod updates may not add or remove containers
[root@master ~]# sed 's,mypod,pod1,' mypod.yaml |kubectl apply -f -
pod/pod1 created

# 在Pod中执行命令
[root@master ~]# kubectl exec myweb -- ls
index.html
info.php
# 在Pod中执行一个交互式命令
[root@master ~]# kubectl exec -it myweb -- bash
[root@myweb html]# : 这里已经进入Pod了

# 拷贝Pod文件到本地
[root@master ~]# mkdir -p website
[root@master ~]# kubectl cp myweb:index.html website/index.html
[root@master ~]# tree website
website
└── index.html
# 拷贝目录到Pod
[root@master ~]# kubectl cp website mypod:./
[root@master ~]# kubectl exec mypod -- tree website
website
`-- index.html

# 删除Pod
[root@master ~]# kubectl delete -f mypod.yaml 
pod "mypod" deleted
[root@master ~]# kubectl delete pod pod1 myweb
pod "pod1" deleted
pod "myweb" deleted
```

### Pod资源文件

#### 使用资源文件定义Pod

```shell
[root@master ~]# vim myhttpd.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: myhttpd
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: apache
    image: myos:httpd
    imagePullPolicy: IfNotPresent
    ports:
    - protocol: TCP
      containerPort: 80

[root@master ~]# kubectl apply -f myhttpd.yaml 
pod/myhttpd created
[root@master ~]# kubectl get pods -o wide
NAME      READY   STATUS    RESTARTS   AGE   IP            NODE
myhttpd   1/1     Running   0          3s    10.244.3.10   node-0003
[root@master ~]# curl http://10.244.3.10
Welcome to The Apache.
```

#### 多容器 Pod

```shell
[root@master ~]# vim mynginx.yaml
---
kind: Pod
apiVersion: v1
metadata:
  name: mynginx
spec:
  containers:
  - name: nginx
    image: myos:nginx
    ports:
    - protocol: TCP
      containerPort: 80
  - name: php
    image: myos:phpfpm
[root@master ~]# kubectl get pods
NAME      READY   STATUS    RESTARTS   AGE
mynginx   2/2     Running   0          2s
# 进入 mynginx 这个 Pod 中，容器 nginx
[root@master ~]# kubectl exec -it mynginx -c nginx -- /bin/bash
# 查看 mynginx 这个 Pod 中，容器 nginx 的日志
[root@master ~]# kubectl logs mynginx -c nginx
# 进入 mynginx 这个 Pod 中，容器 php
[root@master ~]# kubectl exec -it mynginx -c php -- /bin/bash
# 查看 mynginx 这个 Pod 中，容器 php 的日志
[root@master ~]# kubectl logs mynginx -c php
```

#### 排错练习(案例4)

```shell
[root@master ~]# vim myweb.yaml
---
kind: Pod
apiVersion: v1
metadata:
  name: myweb
spec:
  containers:
  - name: httpd
    image: myos:httpd
    ports:
    - protocol: TCP
      containerPort: 80
  - name: nginx
    image: myos:nginx
    ports:
    - protocol: TCP
      containerPort: 80
[root@master ~]# kubectl apply -f myweb.yaml 
pod/myweb created
```

#### 自定义命令

简单命令

```shell
[root@master ~]# vim mycmd.yaml
---
kind: Pod
apiVersion: v1
metadata:
  name: mycmd
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Never
  containers:
  - name: linux
    image: myos:v2009
    imagePullPolicy: IfNotPresent
    command: ["sleep"]
    args: ["10"]

[root@master ~]# kubectl apply -f mycmd.yaml 
pod/mycmd created
[root@master ~]# kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
mycmd   1/1     Running   0          3s
[root@master ~]# kubectl get pods
NAME    READY   STATUS      RESTARTS   AGE
mycmd   0/1     Completed   0          11s

[root@master ~]# kubectl delete -f mycmd.yaml 
pod "mycmd" deleted
```

嵌入式脚本

```shell
[root@master ~]# vim mycmd.yaml
---
kind: Pod
apiVersion: v1
metadata:
  name: mycmd
spec:
  terminationGracePeriodSeconds: 0
  restartPolicy: Always
  containers:
  - name: linux
    image: myos:v2009
    imagePullPolicy: IfNotPresent
    command: ["/bin/bash"]
    args:
    - -c 
    - |
      while true;do
        sleep 5
        echo "hello world."
      done

[root@master ~]# kubectl apply -f mycmd.yaml 
pod/mycmd created
[root@master ~]# kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
mycmd   1/1     Running   0          6s
[root@master ~]# kubectl logs mycmd 
hello world.
hello world.
hello world.

[root@master ~]# kubectl delete -f mycmd.yaml
pod "mycmd" deleted
```

#### 案例6答案

```shell
---
kind: Pod
apiVersion: v1
metadata:
  name: myload
spec:
  terminationGracePeriodSeconds: 0
  containers:
  - name: linux
    image: myos:v2009
    imagePullPolicy: IfNotPresent
    command: ["/bin/bash"]
    args:
    - -c 
    - |
      for((i=20;i>0;i--));do
        X=$(uptime |awk '{printf("%.2f",$(NF-2))}')
        echo "load average: ${X}"
        (( ${X%.*} > 1 )) && break || sleep 15
      done
      exit ${i} 
  restartPolicy: OnFailure
```

